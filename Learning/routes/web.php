<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\ArticleController;
use App\Http\Middleware\MymeType;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/article/', [ArticleController::class,"create"]);

Route::prefix('article')->group(function () {
    Route::get('/', [ArticleController::class,"create"]);
    Route::post('/save', [ArticleController::class,"save"]);
    Route::get('/liste',[ArticleController::class,"list"]);
    Route::get('/{id}-{titre}',[ArticleController::class,"show"]);
    Route::get('/update/{id}/{titre}',[ArticleController::class,"update"])->where(['id'=>'[0-9]+']);
    Route::post('/exeupdate/{id}',[ArticleController::class,"exeupdate"]);
});


Route::middleware('cache.headers:public;max_age=3600;etag')->group(function () {
    Route::get('/ckeditor/{any}', function (Request $request) {
        $path = 'vendor/' . $request->path();
        $path=str_replace('/','\\',$path);
        
        if (File::exists(public_path($path))) {
            $contentType=(new MymeType())->mime_type($path);
            $response = new Illuminate\Http\Response(File::get(public_path($path)), 200);
            $response->header('Content-Type', $contentType);
            return $response;
        } else {
            abort(404);
        }
    })->where('any', '.*');
});
//optional
Route::get('/user/{name?}', function (string $name = null) {
    return $name;
});
 
Route::get('/user/{name?}', function (string $name = 'John') {
    return $name;
});
//regular exprsssion
Route::get('/user/{name}', function (string $name) {
    // ...
})->where('name', '[A-Za-z]+')
->name('username')
;
 
Route::get('/user/{id}', function (string $id) {
    // ...
})->where('id', '[0-9]+');
 
Route::get('/user/{id}/{name}', function (string $id, string $name) {
    // ...
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);

//using the same controller
// Route::controller(OrderController::class)->group(function () {
//     Route::get('/orders/{id}', 'show');
//     Route::post('/orders', 'store');
// });

Route::prefix('admin')->group(function () {
    Route::get('/users', function () {
        // Matches The "/admin/users" URL
    });
});

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

// Route::get('/dashboard', function () {
//     return Inertia::render('Dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

require __DIR__.'/auth.php';
