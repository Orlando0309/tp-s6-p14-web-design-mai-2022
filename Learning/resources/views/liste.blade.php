<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des Articles</title>
    <link href="{{asset('css/style.css') }}" rel="stylesheet">
    <link href="{{asset('css/style2.css') }}" rel="stylesheet">
</head>
<body>
    <h1>Liste des articles</h1>
    <div class="wrapper">
        @foreach($liste as $l)
            
<div class="clash-card barbarian">
      <div class="clash-card__level clash-card__level--barbarian">{{ $l->nomcategorie }}</div>
      <div class="clash-card__unit-name">{{ $l->titre }}</div>
      <div class="clash-card__unit-description">
      {{ $l->resume }}
      </div>

      <div class="clash-card__unit-stats clash-card__unit-stats--barbarian clearfix">
      <a class="button" href="/article/{{$l->id.'-'.$l->slug}}">  
        <div class="one-third">
          <div class="stat">MORE</div>
          <div class="stat-value"></div>
        </a>
        </div>
      </div>
    </div>
        @endforeach
    </div>
</body>
</html>
 